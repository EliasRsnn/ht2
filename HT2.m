% 0452114
% ADAML
% Vilma M�kitalo & Elias R�s�nen
% 16.12.2020
% Practical assignment 2

clear all
close all
clc

%% Hyperparameters
% Data
imageFolder = '..\HT2Data';
inputSize = [224,224];
trainSplit = 0.7;
testSplit = 0.15;
valSplit = 0.15;

% Optimization
baseNetSavePath = '..\HT2Nets';   % Where to store the nets during training
solver = 'adam';
learningRates = [1e-2,1e-3,1e-4];
maxEpochs = 100;
batchSizes = [8,16,32];
LRDropPeriods = [10,50,100];
gradientDecayFactor = 0.9;
freezeFeatureLayers = true;
newLayerLRFactor = 10;

%% Load The data
[allImages,nImage,nClass,classNames] = DataHandling.LoadDataset(imageFolder);
[trainSet,valSet,testSet] = DataHandling.SplitDatasets(allImages,trainSplit,...
                                                       testSplit,valSplit);
                                                   
trainLabels = trainSet.Labels;
valLabels = valSet.Labels;
testLabels = testSet.Labels;

trainSet = DataHandling.AugmentDataset(trainSet,inputSize,...
                                       true);
valSet = DataHandling.AugmentDataset(valSet,inputSize,...
                                     false);
testSet = DataHandling.AugmentDataset(testSet,inputSize,...
                                      false);
%% Training
for learningRate = learningRates
    for batchSize = batchSizes
        for LRDropPeriod = LRDropPeriods
            netSavePath = fullfile(baseNetSavePath,...
                                   [num2str(learningRate),'_',...
                                    num2str(batchSize),'_',...
                                    num2str(LRDropPeriod)]);
            mkdir(netSavePath);
            % Choose the network
            net = NetworkManipulation.CreateNetworkForTransferLearning(mobilenetv2,...
                                                                       false,...
                                                                       nClass,...
                                                                       classNames,...
                                                                       freezeFeatureLayers,...
                                                                       newLayerLRFactor);

            % Training
            
                trainingOpt = trainingOptions(solver,...
                                              'MaxEpochs',maxEpochs,...
                                              'MiniBatchSize',batchSize,...
                                              'Shuffle','every-epoch',...
                                              'ValidationData',valSet,...
                                              'ValidationFrequency',50,...
                                              'InitialLearnRate',learningRate,...
                                              'SquaredGradientDecayFactor',gradientDecayFactor,...
                                              'LearnRateSchedule','piecewise',...
                                              'LearnRateDropPeriod',LRDropPeriod,...
                                              'Plots','training-progress',...
                                              'Verbose',false,...
                                              'CheckPointPath',netSavePath);

            % In retrospect should have saved this
            trainedNet = trainNetwork(trainSet,net,trainingOpt);
        end
    end
end

% Check the accuracies of the checkpoints.
bestAcc = 0;
bestNet = "";
folders = dir(baseNetSavePath);
for folderIndex = 1:size(folders,1)
    folder = folders(folderIndex);
    if strcmp(folder.name,'.') || strcmp(folder.name,'..')
        continue
    end
    disp(folder.name)
    files = dir(fullfile(folder.folder,folder.name));
    hyperParameters = split(folder.name,'_');
    for fileIndex = 1:size(files,1)
        file = files(fileIndex);
        if strcmp(file.name,'.') || strcmp(file.name,'..')
            continue
        end
        net = load(fullfile(file.folder,file.name));
        net = net.net;
        
        % The save format is bad and we need to pass the network through
        % the training. More data in training -> better calibration of the
        % batch normalization layers. One epoch with complete training set
        % ought to do the job
        trainingOpt = trainingOptions(solver,...
                                      'MaxEpochs',1,...
                                      'MiniBatchSize',32,...
                                      'Shuffle','every-epoch',...
                                      'ValidationData',valSet,...
                                      'ValidationFrequency',100,...
                                      'InitialLearnRate',1e-10,...
                                      'SquaredGradientDecayFactor',gradientDecayFactor,...
                                      'LearnRateSchedule','piecewise',...
                                      'LearnRateDropPeriod',100,...
                                      'Verbose',false,...
                                      'ExecutionEnvironment','auto');
        trainedNet = trainNetwork(trainSet,layerGraph(net),trainingOpt);
        acc = ResultEvaluation.CheckAccuracy(net,valSet,valLabels,classNames);
        
        % Display the progress of the best accuracy
        if acc > bestAcc
            bestAcc = acc;
            bestNet = fullfile(file.folder,file.name);
            disp(['net ',fullfile(folder.name,file.name),' gives accuracy ',...
                  num2str(round(acc*100,3))]);
        end
    end
end

% Save the best net such that prediction is immediately possible
net = load(fullfile(file.folder,file.name));
net = net.net;

trainingOpt = trainingOptions(solver,...
                              'MaxEpochs',1,...
                              'MiniBatchSize',32,...
                              'Shuffle','every-epoch',...
                              'ValidationData',valSet,...
                              'ValidationFrequency',100,...
                              'InitialLearnRate',1e-10,...
                              'SquaredGradientDecayFactor',gradientDecayFactor,...
                              'LearnRateSchedule','piecewise',...
                              'LearnRateDropPeriod',100,...
                              'Verbose',false,...
                              'ExecutionEnvironment','auto');

trainedNet = trainNetwork(trainSet,layerGraph(net),trainingOpt);
save('.\asd.mat','trainedNet')

%% Results
net = load('finalNet.mat');
net = net.trainedNet;
acc = ResultEvaluation.CheckAccuracy(net,testSet,testLabels,classNames);
disp(['Test accuracy: ',num2str(round(acc*100,1)),' %']);

ResultEvaluation.ResultPlots(net,testSet,testLabels,classNames);
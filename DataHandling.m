% 0452114
% ADAML
% Vilma M�kitalo & Elias R�s�nen
% 16.12.2020
% Practical assignment 2

classdef DataHandling
    methods(Static)
        function [dataStore,nImage,nClass,classNames] = LoadDataset(imageFolder)
        % Load the images from the given folder. Each label should be in a
        % separate folder with the folder name formatted as xxxxLabel
        % input:
        %   imageFolder     -   Folder that contains the data
        % output:
        %   dataStore       -   ImageDatastore containing the data and the
        %                       labels
        %   nImage          -   Number of samples
        %   nClass          -   Number of classes
        %   classNames      -   String vector containing the names of the classes
            if ~isfolder(imageFolder)
                error(['Couldn''t find folder ',imageFolder]);
            end
            
            dataStore = imageDatastore(imageFolder,...
                                       'IncludeSubfolders',true,...
                                       'LabelSource','foldernames');
            nImage = length(dataStore.Labels);
            
            % Remove the indices from labels and collect the set of labels
            classNames = cell(0,1);
            for label = unique(dataStore.Labels)'
                labelString = char(label);
                cleanLabel = renamecats(dataStore.Labels,labelString,...
                                        labelString(5:end));
                dataStore.Labels = cleanLabel;
                classNames{end+1} = convertCharsToStrings(labelString(5:end));
            end
            classNames = string(classNames);
            nClass = length(classNames);
        end
        
        function [trainSet,valSet,testSet] = SplitDatasets(dataset,trainSplit,...
                                                           testSplit,valSplit)
            % Split the data into train, test and validation data
            % input: 
            %   dataset     -   ImageDatastore containing all of the data
            %   trainSplit  -   Proportion of data to use for training, [0,1]
            %   testSplit   -   Proportion of data to use for testing, [0,1]
            %   valSplit    -   Proportion of data to use for validation, [0,1]
            % output:
            %   trainSet    -   ImageDatastore containing training data
            %   valSet      -   ImageDatastore containing validation data
            %   testSet     -   ImageDatastore containing test data
           
            [trainSet,dataset] = splitEachLabel(dataset,trainSplit);
            [valSet,testSet] = splitEachLabel(dataset,testSplit/(testSplit+valSplit));
            
            % Check that the labels are evenly distributed
            counts = countEachLabel(trainSet).Count;
            if min(counts) ~= max(counts)
                disp('Training set has disproportionate labels')
            end
            counts = countEachLabel(valSet).Count;
            if min(counts) ~= max(counts)
                disp('Validation set has disproportionate labels')
            end
            counts = countEachLabel(testSet).Count;
            if min(counts) ~= max(counts)
                disp('Test set has disproportionate labels')
            end
        end
        
        function [preprocessed] = AugmentDataset(dataset,outputSize,flipAndRotate)
        % Perform data augmentation and format the data such that it can be
        % used in pretrained networks
        % input:
        %   dataset         -   ImageDatastore containing the data
        %   outputSize      -   Desired size of images
        %   flipAndRotate   -   True if you want to randomly flip and
        %                       rotate the images
        % output:
        % preprocessed      -   TransformedDatastore containing the
        %                       augmented data
            if flipAndRotate
                rotationLims = [0,360];
            else
                rotationLims = [0,0];
            end
            augmenter = ...
                imageDataAugmenter('RandXReflection',flipAndRotate,...
                                   'RandYReflection',flipAndRotate,...
                                   'RandRotation',rotationLims);
            
            preprocessed = ...
                augmentedImageDatastore(outputSize,dataset,...
                                        'ColorPreprocessing','rgb2gray',... % The color information cannot be used for classification, as per ch. 2 of the source paper
                                        'DataAugmentation',augmenter,...
                                        'OutputSizeMode','resize');
                  
            transforms = ...
                @(image) DataHandling.CustomTransforms(image);
            preprocessed = transform(preprocessed,transforms);
        end
        
        function batch = CustomTransforms(batch)
        % Perform the transforms when there is no inbuilt capabilities
        % input:
        %   batch   -   Table with input containing the images, and
        %               response containing the label
        % output:
        %   batch   -   batch with transformed input
            for imageIndex = 1:length(batch.input)
                image = batch.input{imageIndex};
                image = cat(3,image,image,image);   % The networks require 3 input channels
                batch.input{imageIndex} = image;
            end
        end
    end
end
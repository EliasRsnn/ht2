% 0452114
% ADAML
% Vilma M�kitalo & Elias R�s�nen
% 16.12.2020
% Practical assignment 2

classdef NetworkManipulation
    methods(Static)
        function [net] = CreateNetworkForTransferLearning(basisNet,...
                                                          printStructure,...
                                                          nClass,...
                                                          classNames,...
                                                          freezeFeatureLayers,...
                                                          newLayerLRFactor)
        % Prepare the pretrained network for transferLearning
        % input:
        %   basisNet                -   DAGNetwork, pretrained net
        %   printStructure          -   True if you want to visualize the
        %                               network
        %   nClass                  -   Number of classes in the dataset
        %   classNames              -   String vector containing the names
        %                               of the classes
        %   freezeFeatureLayers     -   True if you do not want to train
        %                               the feature extraction layers
        %   newLayerLRFactor        -   The factor to use for scaling the
        %                               learning rate in the classication 
        %                               layers
        % output
        %   net                     -   LayerGraph, basisNet with 3 last
        %                               layers replaced by fresh layers
            net = layerGraph(basisNet);
            if printStructure
            	analyzeNetwork(net);
            end
            
            % Change the last three layers
            if freezeFeatureLayers
                fcLayer = fullyConnectedLayer(nClass,...
                                             'Name',"fullyCon");
            else
                fcLayer = fullyConnectedLayer(nClass,...
                                             'Name',"fullyCon",...
                                             'WeightLearnRateFactor',newLayerLRFactor,...
                                             'BiasLearnRateFactor',newLayerLRFactor);
            end
            net = replaceLayer(net,net.Layers(end-2).Name,fcLayer);
            smLayer = softmaxLayer('Name',"softMax");
            net = replaceLayer(net,net.Layers(end-1).Name,smLayer);
            cLayer = classificationLayer('Name',"classifier",'Classes',classNames);
            net = replaceLayer(net,net.Layers(end).Name,cLayer);

            if freezeFeatureLayers
                net = NetworkManipulation.FreezeFeatureLayers(net);
            end
        end
        
        function [net] = FreezeFeatureLayers(net)
        % Set the learning rates to zero in the feature extraction layers
        % input:
        %   net     -    LayerGraph, the network structure
        % output:
        %   net     -   LayerGraph, the network structure with frozen
        %               feature extraction layers
            frozenLayers = net.Layers;
            connections = net.Connections;
            for layerIndex = 1:length(frozenLayers)-3
                if isprop(frozenLayers(layerIndex),"WeightLearnRateFactor")
                    frozenLayers(layerIndex).WeightLearnRateFactor = 0;
                end
                if isprop(frozenLayers(layerIndex),"BiasLearnRateFactor")
                    frozenLayers(layerIndex).BiasLearnRateFactor = 0;
                end
            end
            net = NetworkManipulation.createLgraphUsingConnections(frozenLayers,...
                                                                   connections);
        end
        
        function [lgraph] = createLgraphUsingConnections(layers,connections)
        % Create the network as a graph
        % input:
        %   layers          -   Layer vector containing the layers
        %   connections     -   Table with Source and Destination
        %                       determining the connections with in the
        %                       network. Each row contains the names of the
        %                       source and destination layers
        % output:
        %   lgraph          -   LayerGraph, the network
        % From https://www.mathworks.com/help/deeplearning/ug/train-deep-learning-network-to-classify-new-images.html
            lgraph = layerGraph();
            for i = 1:numel(layers)
                lgraph = addLayers(lgraph,layers(i));
            end

            for c = 1:size(connections,1)
                lgraph = connectLayers(lgraph,connections.Source{c},...
                                       connections.Destination{c});
            end
        end
    end
end
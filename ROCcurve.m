% 0452114
% ADAML
% Vilma M�kitalo & Elias R�s�nen
% 16.12.2020
% Practical assignment 2

function [AUCs, TPRs, FPRs] = ROCcurve(classProbabilities, trueClasses, classIndices)
% Calculate the ROC curves and the areas undeder them
% Inputs
% classProbabilities = Matrix where each column is a sample and each row
% has probability for that sample belonging the corresponding class
% trueClasses = Cellarray of the actual classes of the samples as labels
% classIndices = Cellarray of the poissible classNames.
% Outputs
% AUCs = Vector of area under the curve values of each class.
% TPRs = True positive rates matrix where rows represent different classes
% and columns the TPR value for corresponding threshold value.
% FPRs = False positive rates matrix where rows represent different classes
% and columns the FPR value for corresponding threshold value.

trueInd = labels2Ind(trueClasses, classIndices);

[TPRs, FPRs] = calcTPRandFPR(classProbabilities, trueInd);

AUCs = calsAUCS(TPRs, FPRs);

end

function trueInd = labels2Ind(trueClasses, classIndices)
% Convert the names of the true classes into indices
% Inputs
% trueClasses = Cellarray of the actual classes of the samples as labels
% classIndices = Cellarray of the poissible classNames.
% Outputs
% trueInd = Vector of the indices of the actual classes
    
nSamples = size(trueClasses,1);
nClass = length(classIndices);
trueInd = zeros(nSamples,1);

for i = 1:nSamples
    for j = 1:nClass
        if trueClasses(i) == classIndices(j)
            trueInd(i) = j;
            break;
        end
    end
end

end

function [TPRs, FPRs] = calcTPRandFPR(classProb, trueInd)
% Calculate the True and False Positive Rates
% Inputs
% classProb = Matrix where each column is a sample and each row
% has probability for that sample belonging the corresponding class
% each column
% trueInd = Vector of the indices of the actual classes
% Outputs
% TPRs = True positive rates matrix where rows represent different classes
% and columns the TPR value for corresponding threshold value.
% FPRs = False positive rates matrix where rows represent different classes
% and columns the FPR value for corresponding threshold value.

nClass = max(trueInd);
nSamples = size(classProb,2);

% The decision thresholds
ths = 0:0.01:1;
TPRs = zeros(nClass, length(ths));
FPRs = zeros(nClass, length(ths));

for i = 1:nClass
    
    % Positive
    P = length(find(trueInd == i));
    % Negative
    N = nSamples - P;
    
    for j = 1:length(ths)
        % True positive
        TP = 0; 
        % False positive
        FP = 0;
        for k = 1:nSamples
            if classProb(i,k) > ths(j) && trueInd(k) == i
                TP = TP + 1;
            elseif classProb(i,k) > ths(j) && trueInd(k) ~= i
                FP = FP + 1;
            end
        end

        TPRs(i,j) = TP / P;
        FPRs(i,j) = FP / N;
    end
end

end

function AUCs = calsAUCS(TPRs, FPRs)
% Calculate the areas under the ROC curves
% Inputs
% TPRs = True positive rates matrix where rows represent different classes
% and columns the TPR value for corresponding threshold value.
% FPRs = False positive rates matrix where rows represent different classes
% and columns the FPR value for corresponding threshold value.
% Outputs
% AUCs = Vector of area under the curve values of each class.

nClass = size(TPRs, 1);
nSamples = size(TPRs, 2);
AUCs = zeros(nClass,1);
for i = 1:nClass
    summ = 0;
    for j = 2:nSamples
        a = FPRs(i,j-1);
        b = FPRs(i,j);
        % Trapezoidal integral method
        summ = summ + (a-b) * (TPRs(i,j) + TPRs(i,j-1)) / 2;
    end
    AUCs(i) = summ;
end

end
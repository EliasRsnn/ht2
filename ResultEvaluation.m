% 0452114
% ADAML
% Vilma M�kitalo & Elias R�s�nen
% 16.12.2020
% Practical assignment 2

classdef ResultEvaluation
    methods(Static)
        function [acc] = CheckAccuracy(net,data,labels,classNames)
        % Check the classification accuracy of the net with given data
        % input:
        %   net         -   DAGNetwork, the network
        %   data        -   TransformedDatastore containing the data for
        %                   to perform classification on
        %   labels      -   True classes of data
        %   classNames  -   String vector containing the names of the
        %                   classes
        % output:
        %   acc         -   Proportion of samples that were classified
        %                   right, [0,1]
            truths = labels;
            [~,predLabels] = ResultEvaluation.PredictionWithLabels(net,data,classNames);
            acc = sum(truths == predLabels)/length(predLabels);
        end
        
        function [] = ResultPlots(net,data,labels,classNames)
        % Plot the ROC curves, AUCs for the curves and the confusion matrix
        % for given network and data
        % input:
        %   net         -   DAGNetwork, the network
        %   data        -   TransformedDatastore containing the data for
        %                   to perform classification on
        %   labels      -   True classes of data
        %   classNames  -   String vector containing the names of the
        %                   classes
        
            [preds,predLabels] = ResultEvaluation.PredictionWithLabels(net,data,classNames);
            [AUCs, TPRs, FPRs] = ROCcurve(preds', labels, classNames);
            
            % Plot the mean ROC curve
            figure
            plot(mean(FPRs), mean(TPRs), 'r');
            hold on
            plot([0 1], [0, 1], 'b--');
            legend("Mean ROC", "Random classifier", 'Location', 'southeast');
            title("Mean of every class's ROC curve")
            xlabel("FPR")
            ylabel("TPR")
            axis([-0.1 1.1 -0.1 1.1])
            grid on

            % Plot the AUCS
            figure
            plot(1:length(AUCs), AUCs, 'ro')
            hold on
            plot([1 length(AUCs)], [1/2, 1/2], 'b--')
            legend("AUCs of every class", "Random classifier");
            title("AUCs")
            ylabel("Area")
            xlabel("Class index")
            axis([0 120 0.4 1.2])
            figure
            histogram(AUCs);
            title("Histogram of AUCs")
            xlabel("Area")

            % Display the confusion matrix
            C = confusionmat(labels, predLabels);
            Cper = C / 3;

            figure
            imagesc(Cper)
            title("Confusion matrix")
            hold on
            plot([0, 37.5, 37.5], [37.5, 37.5, 0], 'w') % Division between softwood and hardwood
            xlabel("Predicted class")
            ylabel("True class")
        end
        
        function [preds,predLabels] = PredictionWithLabels(net,data,classNames)
        % Predict the labels of given data
        % input:
        %   net         -   DAGNetwork, the network
        %   data        -   TransformedDatastore containing the data for
        %                   to perform classification on
        %   classNames  -   String vector containing the names of the
        %                   classes
        % output:
        %   preds       -   nSamples x nClasses matrix containing the
        %                   likelihoods [0,1] of each label for each sample
        %   predLabels  -   Predicted labels
            preds = predict(net,data);
            [~,tempLabels] = max(preds,[],2);
            predLabels = cell(length(tempLabels),1);
            for i = 1:length(predLabels)
                predLabels{i} = classNames{tempLabels(i)};
            end
            predLabels = categorical(predLabels);
        end
    end
end